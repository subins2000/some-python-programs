# TorrentBro

A cross-platform Torrent Browser.

## Requirements

* Python 3
* PyQt5
* python-dateutil
* lxml
* purl
* cssselect

## Install

```bash
python3 setup.py install
```

Or if you have the requirements, then you can directly [run](#run) without installing.

## Run

* Clone the source code
* Build the UI & resources :
  ```
  python3 build.py build
  ```
* Run :
  ```
  python3 torrentbro.py
  ```
