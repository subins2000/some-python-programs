'''
Setrix
Copyleft Subin Siby
Licensed under GNU-GPL v3.0
'''

import json
import magic
import os
from PIL import Image

from PyQt5.QtCore import QObject, QThread, pyqtSignal

from matrix_client.client import MatrixClient, Room
from matrix_client.api import MatrixHttpApi, MatrixRequestError
from requests.exceptions import MissingSchema, InvalidSchema

class MatrixWorker(QObject):

    action = matrixClient = matrixAPI = None
    arguments = []
    runs = True

    finished = pyqtSignal(str, object)

    def __init__(self, action, matrixClient, matrixAPI, arguments):
        super().__init__()

        self.action = action
        self.matrixClient = matrixClient
        self.matrixAPI = matrixAPI
        self.arguments = arguments
        self.runs = True

    def stop(self):
        self.runs = False

    def checkIfStopped(self):
        if not self.runs:
            self.quit()
            self.wait()
            self.terminate()
            return True
        else:
            return False

    def _startClient(self):
        settings = self.arguments['settings']

        self.matrixClient = MatrixClient(
            settings.value('homeserver'),
            token=settings.value('token'),
            user_id=settings.value('userID'),
        )

        self.finished.emit(
            self.action,
            self.matrixClient
        )

    def _login(self):
        homeserver = self.arguments['homeserver']

        try:
            client = MatrixClient(homeserver)
            token = client.login_with_password(
                username=self.arguments['username'],
                password=self.arguments['password'],
            )

            self.finished.emit(
                self.action, {
                    'homeserver': homeserver,
                    'token': token,
                    'userID': client.user_id
                }
            )

        except MatrixRequestError as e:
            error = json.loads(e.content)
            self.finished.emit(self.action, {
                'error': error
            })

    def _getRooms(self):
        existingRooms = self.arguments['rooms']
        newRooms = []
        rooms = []

        for roomID, room in self.matrixClient.get_rooms().items():
            roomInfo = {
                'name': 'Unnamed Room',
                'roomID': room.room_id
            }
            try:
                roomInfo['name'] = self.matrixAPI.get_room_name(room.room_id)['name']

                if roomInfo not in existingRooms:
                    newRooms.append(roomInfo)

                rooms.append(roomInfo)

            except MatrixRequestError as e:

                if roomInfo not in existingRooms:
                    newRooms.append(roomInfo)

                rooms.append(roomInfo)

        self.finished.emit(self.action, (rooms, newRooms))

    def _getMessages(self):
        if self.arguments['type'] == 'recent':
            msgs = self.matrixAPI.get_room_messages(
                self.arguments['roomID'],
                self.arguments['token'],
                'b'
            )

            self.finished.emit(self.action, msgs['chunk'])

    def _sendMessage(self):
        if self.arguments['type'] == 'text':

            result = self.matrixAPI.send_message(
                self.arguments['roomID'],
                self.arguments['msg']
            )

            self.finished.emit(self.action, result)

        elif self.arguments['type'] == 'img':

            file = open(self.arguments['location'], 'rb')
            contents = file.read()

            m = magic.open(magic.MIME_TYPE)
            m.load()
            mimeType = m.file(self.arguments['location'])

            contentType = ''
            extraInformation = {}

            if mimeType in ('image/gif', 'image/jpeg', 'image/png', 'image/svg+xml'):
                contentType = 'm.image'

            extraInformation['size'] = os.path.getsize(self.arguments['location'])

            if contentType == 'm.image':
                extraInformation['mimetype'] = mimeType

                img = Image.open(self.arguments['location'])
                extraInformation['w'], extraInformation['h'] = img.size

            try:

                url = self.matrixClient.upload(contents, mimeType)

                result = self.matrixAPI.send_content(
                    self.arguments['roomID'],
                    url,
                    self.arguments['msg'],
                    contentType,
                    extraInformation
                )

                self.finished.emit(self.action, result)

            except MatrixRequestError as e:
                error = json.loads(e.content)
                self.finished.emit(self.action, {
                    'error': error
                })

    def run(self):
        '''
        Call functions that start with '_'
        '''
        actionFunction = getattr(self, '_' + self.action)
        actionFunction()
