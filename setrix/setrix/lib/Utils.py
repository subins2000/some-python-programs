import base64
import requests

class Utils:

    def getAsBase64URL(url):
        response = requests.get(url)
        return "data:" + response.headers['Content-Type'] + ";" + "base64," + base64.b64encode(response.content).decode("utf-8")
