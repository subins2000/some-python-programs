#!/usr/bin/python3

import sys

from PyQt5.QtCore import QTranslator, QLocale, QLibraryInfo
from PyQt5.QtWidgets import QApplication, QWidget

from setrix.Home import *
from setrix.ui import resources


def main():
    app = QApplication(sys.argv)

    home = Home()
    home.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
