# Interested

* Vishnu K ✔
* Nirag
* Jadhesh ✔
* Vijina ✔
* Subin ✔
* Neelima
* Shafi ✔
* Kiran
* Shyam
* Sachin ✔
* Elvis ✔
* Sonu ✔
* Sanil ✔
* Suraj ✔
* Nived N ✔

# Submitted

| Name | Count | Program
| ---- | ----- | -------
| Jadhesh | 1 | JourneyToThePrincess.py (with an alternate GUI version)
| Vijina | 2 | HangmanAuto.py, HangmanManual.py
| Sonu | 2 | MATRIX8.py, MessageEncryptor.py
| Subin | 2 | setrix, TorrentBro
| Vishnu | 1 | Owl
| Sachin | 1 | SnakeGame.py
| Elvis | 1 | Hangman (directory)
| Sanil | 1 | Restaurant.py
| Suraj | 1 | Suraj.py
| Nived | 1 | Nived.py
| Shafi | 1 | OCR/new/new.py (not in git)
| Jessica | 1 | SequenceGame.py
| Sherin | 1 | MemoryGame.py
| Siya | 1 | ColourGame.py
| Mrudhula | 1 | TicTacToe.py
| Total | 18 projects |
| Unique projects | 15 projects |
